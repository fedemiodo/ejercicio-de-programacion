// testing manual
/*
// CHEQUEAR EXISTENCIA DE INSTANCIA DE CLASE

empresa = new facturacion_general(10,"Cordoba");

// CHEQUEAR EXISTENCIA DE FACTURA

empresa.visualizarFactura(2,2021);

// CREAR NUEVAS LLAMADAS NO LOCALES EXITOSAS Y VERIFICAR FACTURA

empresa.agregarLlamada("Buenos Aires",60);
empresa.agregarLlamada("Villa Carlos Paz", 120);
empresa.visualizarFactura(2,2021);

// CREAR NUEVAS LLAMADAS NO EXITOSAS Y COMPROBAR ERROR

empresa.agregarLlamada("Rusia", 20);
empresa.agregarLlamada("Buenos Aire", 100);
empresa.visualizarFactura(2,2021);

// CREAR LLAMADAS LOCALES Y VERIFICAR FACTURA

empresa2 = new facturacion_general(10, "Buenos Aires");
empresa2.agregarLlamada("Buenos Aires", 120);
empresa2.visualizarFactura(2,2021);

// AGREGAR DESTINO Y LLAMAR AL MISMO LOCALMENTE

empresa = new facturacion_general(10,"Cordoba");
empresa.agregarDestino("Cordoba", 0.5);
empresa.agregarLlamada("Cordoba", 100);
empresa.visualizarFactura(2,2021);

// AGREGAR DESTINO Y LLAMAR AL MISMO NO-LOCALMENTE

empresa = new facturacion_general(10,"Buenos Aires");
empresa.agregarDestino("Cordoba", 0.5);
empresa.agregarLlamada("Cordoba", 100);
empresa.visualizarFactura(2,2021);


TODOS LOS TESTS FUERON EXITOSOS
*/
