
// Costo por minuto de distintos destinos
let costos_especiales = new Object();

// Predeterminados:

costos_especiales["Buenos Aires"] = 0.2;
costos_especiales["Villa Carlos Paz"] = 0.3;
costos_especiales["Bariloche"] = 0.5;
costos_especiales["Chile"] = 0.4;
costos_especiales["Islandia"] = 0.6;
costos_especiales["Canada"] = 0.7;

class facturacion_del_mes {
    #_mes_actual;
    #_anio_actual;
    #_abono_basico;
    #_abono_llamadas_locales = 0;
    #_abono_llamadas_nolocales = 0;
    #_localidad;
    
    // Constructor
    constructor(abono_mensual, localidad) {
        this.#_mes_actual = new Date().getMonth();
        this.#_anio_actual = new Date().getFullYear();
        this.#_abono_basico = abono_mensual;
        this.#_localidad = localidad;
    }

    // Metodos de clase
    agregarLlamada(destino, minutos) {
        const mes = new Date().getMonth();
        if (mes == this.#_mes_actual) {
            const dia = new Date().getDay();
            const hora = new Date().getHours();
            if (destino == this.#_localidad) {
                (hora > 8 && hora < 20 && (dia == 1 || dia == 2 || dia == 3 || dia == 4 || dia == 5))
                ? this.#_abono_llamadas_locales += minutos * 0.2
                : this.#_abono_llamadas_locales += minutos * 0.1;
            } else {
                let costo_actual = minutos * costos_especiales[destino];
                !(isNaN(costo_actual)) ? this.#_abono_llamadas_nolocales += costo_actual : console.error('destino indicado no se encuentra en la base de datos');
            }
        } else {
            console.error('intento de agregar una llamada a un mes anterior al actual.');
        }        
    }
    visualizarFactura() {
        console.log(`Factura del ${this.#_mes_actual}/${this.#_anio_actual}:`);
        console.log(`Abono basico: ${this.#_abono_basico}`);
        console.log(`Abono llamadas locales: ${this.#_abono_llamadas_locales}`);
        console.log(`Abono llamadas no locales: ${this.#_abono_llamadas_nolocales}`);
        console.log('...')
        console.log(`Total factura: ${this.#_abono_basico + this.#_abono_llamadas_locales + this.#_abono_llamadas_nolocales}`);
    }
    mesDeFacturacion() {
        return {
            mes : this.#_mes_actual,
            anio : this.#_anio_actual
        }
    }
}


class facturacion_general {
    #_abono_basico;
    #_fecha_creacion;
    #_localidad;
    #_meses_guardados = [];
    
    // Constructor
    constructor(abono_mensual, localidad) {
        this.#_abono_basico = abono_mensual;
        this.#_localidad = localidad;
        this.#_fecha_creacion = new Date();
        const primer_mes = new facturacion_del_mes(abono_mensual, localidad);
        this.#_meses_guardados.push(primer_mes);
    }

    // Metodos de clase
    visualizarFactura(mes,anio) {
        this.#_meses_guardados.forEach(element => {
            if (element.mesDeFacturacion().mes == mes && element.mesDeFacturacion().anio == anio) {
                return element.visualizarFactura();
            }
        });
        return 0;
    }

    cantidadDeFacturas() {
        return this.#_meses_guardados.length;
    }

    agregarLlamada(destino, minutos) {
        const mes = new Date().getMonth();
        const anio = new Date().getFullYear();
        if (this.#_meses_guardados[this.cantidadDeFacturas() - 1].mesDeFacturacion().mes == mes &&
        this.#_meses_guardados[this.cantidadDeFacturas() - 1].mesDeFacturacion().anio == anio) {  // si se trata de un mes ya existente en la base de datos
            this.#_meses_guardados[this.cantidadDeFacturas() - 1].agregarLlamada(destino,minutos);
        } else {
            this.#_meses_guardados.append(new facturacion_del_mes(this.#_abono_basico, this.#_localidad));
            this.#_meses_guardados[this.cantidadDeFacturas() - 1].agregarLlamada(destino,minutos);
        }
    }
    agregarDestino(destino,precio_por_minuto) {
        costos_especiales[destino] = precio_por_minuto;
    }
}
