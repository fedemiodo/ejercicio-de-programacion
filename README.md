# Ejercicio de programacion

El lenguaje utilizado para resolver el ejercicio es Vanilla JS.

Para testear la funcionalidad, se abre el archivo index.html en un browser (ej. Google Chrome) y se ejecuta la consola del mismo,
en la cual se testean las funcionalidades descriptas en el archivo llamadas_v2.test.js


Se intentó realizar testeo por software utilizando Jest, una libreria de Node JS, pero no se logró la compatibilidad entre esta librería
y los módulos ES6 que provee Node.

---
Decisiones al implementar el problema:
1) el control del tiempo es en tiempo real, utilizando el objeto Date() innato de JS.
2) las llamadas se cargan al sistema al efectuarse, no es posible cargar una llamada de, por ejemplo, el día anterior. (podría implementarse en un futuro)
3) los destinos nacionales e internacionales deben agregarse manualmente, se implementa la funcionalidad para hacerlo. Se pueden encontrar algunos predeterminados.
4) las facturas guardadas se pueden visualizar en cualquier momento, si el mes corriente no se encuentra finalizado se verá una factura "en curso", es decir, sujeta a posibles cambios hasta finalizar el mes.

---
Errores encontrados tras el commit del 3/3:
1) De no haber llamados en un mes entero que no sea el primero, no se genera la factura del mismo.
